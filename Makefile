##
## EPITECH PROJECT, 2017
## Makefile
## File description:
## Makefile
##

NAME	=	my_libs

NAMET	=	libs_tests

CC	=	gcc -Iinclude/

SRC	=	main.c

SRCT	=	$(wildcard tests/*/*.c)

CFLAGS	=	-W -Wall -Wextra

OBJ		=	$(SRC:.c=.o)

OBJT	=	$(wildcard *.o)

LIB_DIR	=	./

LDFLAGS	=	-L$(LIB_DIR) -lmy_libc -ldoublechar -lsorting -llists -lmy_ext_libc -lmy_files

all:	$(NAME)

lib_make:
	make -C $(LIB_DIR)my_libc
	make -C $(LIB_DIR)my_ext_libc
	make -C $(LIB_DIR)doublechar
	make -C $(LIB_DIR)lists
	make -C $(LIB_DIR)sorting
	make -C $(LIB_DIR)my_files

$(NAME):	 lib_make $(OBJ)
	$(CC) -o $(NAME) $(CFLAGS) $(OBJ) $(LDFLAGS)

tests_run:	lib_make
	$(CC) --coverage -c $(SRCT)
	$(CC) $(CFLAGS) $(OBJT) -lcriterion -lgcov -o $(NAMET) $(LDFLAGS)
	./$(NAMET)

clean:
	make clean -C $(LIB_DIR)my_libc
	make clean -C $(LIB_DIR)my_ext_libc
	make clean -C $(LIB_DIR)doublechar
	make clean -C $(LIB_DIR)lists
	make clean -C $(LIB_DIR)sorting
	make clean -C $(LIB_DIR)my_files
	$(RM) $(OBJ) $(OBJT)

fclean:	clean
	make fclean -C $(LIB_DIR)my_libc
	make fclean -C $(LIB_DIR)my_ext_libc
	make fclean -C $(LIB_DIR)doublechar
	make fclean -C $(LIB_DIR)lists
	make fclean -C $(LIB_DIR)sorting
	make fclean -C $(LIB_DIR)my_files
	$(RM) $(NAME) $(NAMET)

re:	fclean all

.PHONY: all, clean, fclean, re, tests_run, lib_make

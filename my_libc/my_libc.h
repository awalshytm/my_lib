
 #pragma once

 /* int */
int my_atoi(const char *);
// int my_pow(int, int);
// int my_sqrt(int);
// int my_strcmp(char *, char *);
// int my_strncmp(char *, char *, int);
int my_strlen(const char *);

/* display and void */
void my_swap(int *, int *);
void my_putstr(const char *);
void my_putchar(char);
void my_puterr(const char *);
void my_putnbr_base(int, const char *);
void my_putnbr(int);
// void my_sort_int_array(int *, int, int);

/* char star */
char *my_revstr(char *);
// char *my_strlowcase(char *);
// char *my_struppercase(char *);
// char *mystrcat(char *, char *);
// char *my_strcpy(char *, char *);
// char *my_strdupf(char *);
// char *my_strdup(char *);
// char *my_strncpy(char *, char *, int);
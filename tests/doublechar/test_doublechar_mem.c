#include "doublestar.h"
#include <criterion/criterion.h>

Test(doublechar, dup)
{
    char **str = malloc(sizeof(char *) * 3);

    str[0] = strdup("arthur");
    str[1] = strdup("walsh");
    str[2] = NULL;
    new = doublechar_dup(str);
    cr_assert_str_eq(str[0], new[0], "doublechar dup str[0]");
    cr_assert_str_eq(str[1], new[1], "doublechar dup str[1]");
    cr_assert_not(&str, &new, "doublechar dup pointers");
}

Test(doublechar, free)
{
    char **str = malloc(sizeof(char *) * 3);

    str[0] = strdup("arthur");
    str[1] = strdup("walsh");
    str[2] = NULL;
    doublestar_free(str);
    cr_assert_eq(str, NULL, "doublechar free");
}
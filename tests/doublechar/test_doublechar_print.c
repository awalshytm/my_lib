#include "doublechar.h"
#include <criterion/criterion.h>

void redirect_all_std(void)
{
    cr_redirect_stdout();
    cr_redirect_stderr();
}

Test(doublechar, prints, .init = redirect_all_std)
{
    char **str = malloc(sizeof(char *) * 3);

    str[0] = strdup("arthur ");
    str[1] = strdup("walsh");
    str[2] = NULL;
    doublechar_display(str);
    cr_assert_stdout_eq_str("arthur walsh", "display doublechar");
}
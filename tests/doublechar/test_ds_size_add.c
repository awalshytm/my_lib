#include "doublestar.h"
#include <criterion/criterion.h>

Test(doublechar, size)
{
    char **test = malloc(sizeof(char *) * 3);

    test[0] = strdup("arthur");
    test[1] = strdup("walsh");
    test[2] = NULL;
    cr_assert_eq(doublechar_size(test), 2, "doublechar size");
    cr_assert_eq(doublechar_length(test), 11, "doublechar length");
}

Test(doublechar, add_elem_front)
{
    char **test = malloc(sizeof(char *) * 3);

    test[0] = strdup("arthur");
    test[1] = strdup("walsh");
    test[2] = NULL;
    test = doublechar_add_elem_front(test, "bonjour");
    cr_assert_eq(doublechar_size(test), 3, "doublechar add_elem_front size");
    cr_assert_eq(doublechar_length(length), 18, "doublechar add_elem_front length");
    cr_assert_str_eq(test[0], "bonjour", "doublechar_add_elem_front str[0]");
    cr_assert_str_eq(test[2], "walsh", "doublechar_add_elem_front str[2]");
}

Test(doublechar, add_elem_back)
{
    char **test = malloc(sizeof(char *) * 3);

    test[0] = strdup("arthur");
    test[1] = strdup("walsh");
    test[2] = NULL;
    test = doublechar_add_elem_back(test, "bonjour");
    cr_assert_eq(doublechar_size(test), 3, "doublechar add_elem_back size");
    cr_assert_eq(doublechar_length(length), 18, "doublechar add_elem_back length");
    cr_assert_str_eq(test[2], "bonjour", "doublechar_add_elem_back str[2]");
    cr_assert_str_eq(test[0], "arthur", "doublechar_add_elem_back str[0]");
}
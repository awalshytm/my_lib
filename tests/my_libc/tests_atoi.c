#include "my_libs.h"
#include <criterion/criterion.h>

Test(my_atoi, positive)
{
    char *str = "123";
    char *str1 = "146789";
    char *str2 = "2147483647";

    cr_assert_eq(my_atoi(str), 123, "atoi: 123");
    cr_assert_eq(my_atoi(str1), 146789, "atoi: 146789");
    cr_assert_eq(my_atoi(str2), 2147483647, "atoi: 2147483647");
}

Test(my_atoi, negative)
{
    char *str = "-123";
    char *str1 = "-146789";
    char *str2 = "-2147483648";

    cr_assert_eq(my_atoi(str), -123, "atoi: -123");
    cr_assert_eq(my_atoi(str1), -146789, "atoi: -146789");
    cr_assert_eq(my_atoi(str2), -2147483648, "atoi: -2147483648");
}

Test(my_atoi, char_before)
{
    char *str = "abc123";
    char *str1 = "arthur146789";
    char *str2 = "   2147483647";

    cr_assert_eq(my_atoi(str), 0, "atoi: abc123");
    cr_assert_eq(my_atoi(str1), 0, "atoi: arhur146789");
    cr_assert_eq(my_atoi(str2), 2147483647, "atoi: '   2147483647'");
}

Test(my_atoi, char_after)
{
    char *str = "123xyz";
    char *str1 = "146789walsh";
    char *str2 = "2147483647     ";

    cr_assert_eq(my_atoi(str), 123, "atoi: 123");
    cr_assert_eq(my_atoi(str1), 146789, "atoi: 146789");
    cr_assert_eq(my_atoi(str2), 2147483647, "atoi: '2147483647    '");
}

Test(my_atoi, char_middle)
{
    char *str = "12  3";
    char *str1 = "146abc789";
    char *str2 = "  21474ab83647cb";

    cr_assert_eq(my_atoi(str), 12, "atoi: 12  3");
    cr_assert_eq(my_atoi(str1), 146, "atoi: 146abc789");
    cr_assert_eq(my_atoi(str2), 21474, "atoi: '  21474ab83647cb'");
}
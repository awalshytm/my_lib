#include "my_libs.h"
#include <criterion/criterion.h>
#include <stdlib.h>

Test(my_strlowcase, basic_char)
{
    cr_assert_str_eq(my_strlowcase("ARTHUR"), "arthur", "strlowcase: ARTHUR");
    cr_assert_str_eq(my_strlowcase("BoNjOuR"), "bonjour", "strlencase: BoNjOuR");
    cr_assert_str_eq(my_strlowcase("coucou"), "coucou", "strlen: coucou");
}

Test(my_strlowcase, special_char)
{
    cr_assert_str_eq(my_strlowcase("ARTHUR\n"), "arthur\n", "strlowcase: arthur \\n");
    cr_assert_str_eq(my_strlowcase("\tArThUr**"), "\tarthur**", "strlowcase: \\tArThUr**");
    cr_assert_str_eq(my_strlowcase(""), "", "strlowcase: ''");
    cr_assert_str_eq(my_strlowcase(NULL), NULL, "strlowcase: NULL");
}

Test(my_struppercase, basic_char)
{
    cr_assert_str_eq(my_struppercase("arthur"), "ARTHUR", "struppercase: ARTHUR");
    cr_assert_str_eq(my_struppercase("BoNjOuR"), "BONJOUR", "struppercase: BoNjOuR");
    cr_assert_str_eq(my_struppercase("COUCOU"), "COUCOU", "struppercase: COUCOU");
}

Test(my_struppercase, special_char)
{
    cr_assert_str_eq(my_struppercase("arthur\n"), "ARTHUR\n", "struppercase: arthur \\n");
    cr_assert_str_eq(my_struppercase("\tArThUr**"), "\tARTHUR**", "struppercase: \\tArThUr**");
    cr_assert_str_eq(my_struppercase(""), "", "struppercase: ''");
    cr_assert_str_eq(my_struppercase(NULL), NULL, "struppercase: NULL");
}
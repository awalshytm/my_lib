#include "my_libs.h"
#include <criterion/criterion.h>
#include <stdlib.h>
#include <string.h>

Test(my_strcpy, basics)
{
    char *result = malloc(sizeof(char) * 7);

    result = my_strcpy(result, "arthur");
    cr_assert_str_eq(result, "arthur", "strcpy: arthur");
    result = my_strcpy(result, NULL);
    cr_assert_str_eq(result, NULL, "strcpy: NULL");
    result = malloc(0);
    result = my_strcpy(result, "");
    cr_assert_str_eq(result, "", "strcpy: ''");
}

Test(my_strcat, bascis)
{
    char *res = NULL;

    cr_assert_str_eq(my_strcat("arthur", " walsh"), "arthur walsh", "strcat: 'arthur walsh'");
    cr_assert_str_eq(my_strcat("", "arthur"), "arthur", "strcat: '' 'arthur'");
    cr_assert_str_eq(my_strcat(NULL, "arthur"), NULL, "strcat: NULL arthur");
}

Test(my_strdup, basics)
{
    char *str = strdup("arthur");
    char *res = my_strdup(str);

    cr_assert_str_eq(str, res, "strdup: strings");
    cr_assert_not(&str == &res, "strdup: pointers");
}

Test(my_strdup, null_empty)
{
    char *str = "";
    char *res = my_strdup(str);

    cr_assert_str_eq(str, res, "strdup: '' string");
    cr_assert_not(&str == &res, "strdup: '' pointer");
    str = NULL;
    res = my_strdup(str);
    cr_assert_str_eq(str, res, "strdup: NULL string");
    cr_assert_not(&str == &res, "strdup: NULL pointer");
}
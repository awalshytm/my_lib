#include "my_libs.h"
#include <criterion/criterion.h>

Test(my_pow, v1)
{
    cr_assert_eq(my_pow(2, 3), 8, "pow: 2 3 = 8");
    cr_assert_eq(my_pow(2, 2), 4, "pow: 2 2 = 4");
    cr_assert_eq(my_pow(2, 1), 2, "pow: 2 1 = 2");
    cr_assert_eq(my_pow(2, 0), 1, "pow: 2 0 = 1");
    cr_assert_eq(my_pow(0, 2), 0, "pow: 0 2 = 0");
}

Test(my_sqrt, v1)
{
    cr_assert_eq(my_sqrt(16), 4, "sqrt 16");
    cr_assert_eq(my_sqrt(0), 0, "sqrt 0");
    cr_assert_eq(my_sqrt(2), 0, "sqrt 2");
}
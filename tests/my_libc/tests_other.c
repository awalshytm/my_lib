#include "my_libs.h"
#include <criterion/criterion.h>
#include <string.h>

Test(my_revstr, v1)
{
    char *arthur = strdup("arthur");
    char *walsh = strdup("walsh\n");
    char *tab = strdup("t\tab");

    cr_assert_str_eq(my_revstr(arthur), "ruhtra", "revstr: arthur");
    cr_assert_str_eq(my_revstr(walsh), "\nhslaw", "revstr: walsh\\n");
    cr_assert_str_eq(my_revstr(tab), "ba\tt", "revstr: tab");
}

Test(my_swap, v1)
{
    int a = 5;
    int b = -5;

    my_swap(&a, &b);
    cr_assert_eq(a, -5, "swap a 5 -5");
    cr_assert_eq(b, 5, "swap b 5 -5");
    a = 2147483647;
    b = 10;
    my_swap(&a, &b);
    cr_assert_eq(a, 10, "swap a 2147483647 10");
    cr_assert_eq(b, 2147483647, "swap b 2147483647 10");
}
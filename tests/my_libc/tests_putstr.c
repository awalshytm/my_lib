#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my_libs.h"

void redirect_all_std(void)
{
    cr_redirect_stdout();
    cr_redirect_stderr();
}

Test(my_putstr, basics, .init = redirect_all_std)
{
    my_putstr("arthur");
    cr_assert_stdout_eq_str("arthur\n", "putstr: arthur");
    my_putstr("arthur\n");
    cr_assert_stdout_eq_str("arthur\n", "putstr: arthur\\n");
    my_putstr("arthur\t");
    cr_assert_stdout_eq_str("arthur\n", "putstr: arthur\\t ");
}

Test(my_puterr, basics, .init = redirect_all_std)
{
    my_puterr("ERROR!");
    cr_assert_stderr_eq_str("ERROR\n", "puterr: ERROR");
    my_puterr("ERROR\n");
    cr_assert_stderr_eq_str("ERROR\n", "puterr: ERROR\\n");
}

Test(my_puts, null, .init = redirect_all_std)
{
    my_putstr(NULL);
    cr_assert_stdout_eq_str("", "putstr: NULL");
    my_puterr(NULL);
    cr_assert_stderr_eq_str("", "puterr: NULL");
}

Test(my_putchar, basics, .init = redirect_all_std)
{
    my_putchar('a');
    cr_assert_stdout_eq_str("a", "putchar: a");
}

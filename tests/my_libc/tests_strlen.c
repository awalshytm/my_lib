#include "my_libs.h"
#include <criterion/criterion.h>

Test(my_strlen, normal_strings)
{
    char *arthur = "arthur";
    char *walsh = "walsh";
    char *longstr = "this is actually a long phrase";

    cr_assert_eq(my_strlen(arthur), 6, "strlen: arthur");
    cr_assert_eq(my_strlen(walsh), 5, "strlen: walsh");
    cr_assert_eq(my_strlen(longstr), 30, "strlen: longstring");
}

Test(my_strlen, empty)
{
    cr_assert_eq(my_strlen(""), 0, "strlen: ''");
    cr_assert_eq(my_strlen(NULL), -1, "strlen: NULL");
}

Test(my_strlen, special_char)
{
    char *backslashn = "\n";
    char *backslashn2 = "word\n";
    char *tab = "\t";
    char *tab2 = "tab\t";
    char *star = "star*";

    cr_assert_eq(my_strlen(backslashn), 1, "strlen: backslashn");
    cr_assert_eq(my_strlen(backslashn2), 5, "strlen: backslashn2");
    cr_assert_eq(my_strlen(tab), 1, "strlen: tab");
    cr_assert_eq(my_strlen(tab2), 4, "strlen: tab2");
    cr_assert_eq(my_strlen(star), 5, "strlen: star");
}

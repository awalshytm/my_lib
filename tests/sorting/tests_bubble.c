#include "my_libs.h"
#include <criterion/criterion.h>

Test(bubble_sort, v1)
{
    int *tab1 = {23, 1, 45, 74, 1200};
    int *expected1 = {1, 23, 45, 74, 1200};

    int *tab2 = {-1, -5, 0, -56, -123, 1};
    int *expected2 = {-123, -56, -5, -1, 0};

    bubble_sort(tab1);
    bubble_sort(tab2);
    cr_assert_arr_eq(tab1, expected1, "bubble sort 1 positives");
    cr_assert_arr_eq(tab2, expected2, "bubble sotr 2 negatives");
}

